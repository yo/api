import type { NextPage } from "next";

const Home: NextPage = () => {
  return <a href="https://yogi.codes">Visit yogi.codes</a>;
};

export default Home;
