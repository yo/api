import axios from "axios";
import { NextApiRequest, NextApiResponse } from "next";

const handler = async (req: NextApiRequest, res: NextApiResponse) => {
  const LASTFM_USERNAME = "yoginth";
  const LASTFM_API_KEY = process.env.LASTFM_API_KEY;

  try {
    const get_track = await axios.get(
      `http://ws.audioscrobbler.com/2.0/?method=user.getrecenttracks&user=${LASTFM_USERNAME}&api_key=${LASTFM_API_KEY}&format=json`
    );

    const track = get_track.data["recenttracks"]["track"];
    const playing = track[0]["@attr"] !== undefined;

    res.setHeader("Cache-Control", "s-maxage=50");
    return res.status(200).json({
      name: track[0]["name"],
      artist: track[0]["artist"]["#text"],
      url: track[0]["url"],
      image: track[0]["image"][3]["#text"],
      playing
    });
  } catch (err) {
    return res.status(500).json({ message: "💩" });
  }
};

export default handler;
