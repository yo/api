import axios from "axios";
import { NextApiRequest, NextApiResponse } from "next";

const handler = async (req: NextApiRequest, res: NextApiResponse) => {
  const GITLAB_TOKEN = process.env.GITLAB_TOKEN;

  try {
    const response = await axios.get(
      `https://gitlab.com/api/v4/projects/23590604/issues?access_token=${GITLAB_TOKEN}`
    );
    const data = response.data[0];

    res.setHeader("Cache-Control", "s-maxage=3600");
    return res.status(200).json({
      mood: data.title,
      created_at: data.created_at,
    });
  } catch (err) {
    return res.status(500).json({ message: "💩" });
  }
};

export default handler;
