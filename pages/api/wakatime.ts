import axios from "axios";
import { NextApiRequest, NextApiResponse } from "next";

const handler = async (req: NextApiRequest, res: NextApiResponse) => {
  const WAKATIME_API_KEY = process.env.WAKATIME_API_KEY;

  try {
    const response = await axios.get(
      `https://wakatime.com/api/v1/users/current/stats/last_7_days?api_key=${WAKATIME_API_KEY}`
    );
    const data = response.data["data"];

    res.setHeader("Cache-Control", "s-maxage=3600");
    return res.status(200).json({
      total: data.human_readable_total,
      updated_at: data.modified_at,
    });
  } catch (err) {
    return res.status(500).json({ message: "💩" });
  }
};

export default handler;
